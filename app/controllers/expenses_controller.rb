class ExpensesController < EntriesController

  def create
    @entry = Expense.new(entry_params)
    @entry.user = current_user

    if @entry.save
      redirect_to root_path, notice: t("entry_created.expenses")
    else
      redirect_to root_path, notice: @entry.errors.full_messages.join(". ")
    end
  end

  def update
    if resource.update_attributes(entry_params)
      redirect_to user_entries_path(current_user), notice: t("entry_saved")
    else
      redirect_to edit_expense_path(resource), notice: t("entry_failed")
    end
  end

  def edit
    super
    resource
  end

  private

  def resource
    @expenses_entry ||= current_user.expenses.find(params[:id])
  end

  def entry_params
    params.require(:expense).
      permit(:project_id, :category_id, :amount, :description, :date).
      merge(date: parsed_date(:expense))
  end



#  before_action :set_expense, only: [:show, :edit, :update, :destroy]

  # GET /expenses
  def index
   @expenses = Expense.all
  end

  # GET /expenses/1
 # def show
 # end

  # GET /expenses/new
 # def new
  #  @expense = Expense.new
 # end

  # GET /expenses/1/edit
 # def edit
 # end

  # POST /expenses
 # def create
  #  @expense = Expense.new(expense_params)

   # if @expense.save
    #  redirect_to @expense, notice: 'Expense was successfully created.'
  #  else
   #   render :new
    #end
 # end

  # PATCH/PUT /expenses/1
 # def update
  #  if @expense.update(expense_params)
   #   redirect_to @expense, notice: 'Expense was successfully updated.'
  #  else
   #   render :edit
   # end
 # end

  # DELETE /expenses/1
#  def destroy
 #   @expense.destroy
  #  redirect_to expenses_url, notice: 'Expense was successfully destroyed.'
 # end

 # private
    # Use callbacks to share common setup or constraints between actions.
  #  def set_expense
   #   @expense = Expense.find(params[:id])
   # end

    # Only allow a trusted parameter "white list" through.
   # def expense_params
    #  params.require(:expense).permit(:amount)
   # end
end
