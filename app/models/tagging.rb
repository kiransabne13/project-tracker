# == Schema Information
#
# Table name: taggings
#
#  id         :integer          not null, primary key
#  tag_id     :integer
#  hour_id    :integer
#  created_at :datetime
#  updated_at :datetime
#  expense_id :integer

class Tagging < ActiveRecord::Base
  belongs_to :tag
  belongs_to :hour
  belongs_to :expense
  validates :tag, presence: true
  validates :hour, presence: true
  validates :expense, presence: true
end
