class CreateExpenses < ActiveRecord::Migration
  def change
    create_table :expenses do |t|
      t.decimal :amount
      t.references :project, index: true, null: false
      t.references :user, index: true, null: false
      t.date :date, null: false
      t.boolean :billed, default: false
      t.timestamps null: false
      t.references :category, index: true, null: false
      t.datetime :created_at
      t.datetime :updated_at
      t.string :description, null: false
    end
    add_index(:expenses, :billed)
    add_index(:expenses, :date)
  end
end
