class AddExpenseIdToTaggings < ActiveRecord::Migration
  def change
    add_column :taggings, :expense_id, :integer
    add_index :taggings, :expense_id
  end
end
